# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

s = Subject.create(name: 'Sales')

5.times do |i|
  s.questions.create([{
    position: i+1,
    time_limit: 60*5,
    title: Faker::RickAndMorty.quote,
    description: 3.times.map { Faker::ChuckNorris.fact }.join(' ')
  }])
end

num_users = User.count
u = User.create(
  first_name: Faker::Name.first_name,
  last_name: Faker::Name.last_name,
  email: "#{Faker::Name.first_name}_#{num_users}@#{Faker::Name.last_name}.fake",
)

s.questions.each do |question|
  a = question.answers.new(user_id: u.id)
  time_taken = question.time_limit - rand(1..15)

  a.update(
    response: Faker::RickAndMorty.quote,
    time_taken: time_taken < 0 ? 0 : time_taken
  )
end
