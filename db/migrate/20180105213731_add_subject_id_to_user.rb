class AddSubjectIdToUser < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :subject_id, :integer
    add_index :users, :subject_id
  end
end
