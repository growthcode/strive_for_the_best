class CreateQuestions < ActiveRecord::Migration[5.1]
  def change
    create_table :questions do |t|
      t.string  :title
      t.text    :description
      t.integer :time_limit # in seconds
      t.integer :subject_id
      t.integer :position

      t.timestamps
    end
    add_index :questions, :subject_id
  end
end
