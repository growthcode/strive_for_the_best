class CreateAnswers < ActiveRecord::Migration[5.1]
  def change
    create_table :answers do |t|
      t.text :response
      t.integer :time_taken # in seconds
      t.integer :question_id, null: false
      t.integer :user_id, null: false

      t.timestamps
    end
    add_index :answers, :question_id
    add_index :answers, :user_id
  end
end
