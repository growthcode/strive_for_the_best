Rails.application.routes.draw do
  # =========================================
  # for internal strive app (not part of test)
  # namespace :admin do
  #   resources :answers
  #   resources :questions
    # resources :subjects
  # end
  # =========================================

  namespace :test do
    # to do use subject name as slug
    resources :subject, only: [] do
      get 'questions/:position', to: 'questions#show', as: 'ask_question'
      post 'question/:position', to: 'questions#update', as: 'submit_question'
    end
  end

  # resources :test, only: [:edit, :update]
  post 'landing/sign_in', to: 'landing#sign_in', as: 'sign_in'
  root to: 'landing#index'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
