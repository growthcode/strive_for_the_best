# == Schema Information
#
# Table name: users
#
#  id                 :integer          not null, primary key
#  first_name         :string
#  last_name          :string
#  email              :string
#  terms_confirmation :boolean
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#

class User < ApplicationRecord
  has_many :answers
  has_many :questions, through: :answers

  normalize_attributes :first_name, :last_name, with: :squish
  normalize_attributes :email, with: [:strip] do |value|
    value.try(:downcase).present? ? value.try(:downcase) : nil
  end

  validates :email, :first_name, :last_name, presence: true
  validates_format_of :email, with: Devise::email_regexp, if: 'email.present?'
end
