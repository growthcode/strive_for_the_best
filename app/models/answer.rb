# == Schema Information
#
# Table name: answers
#
#  id          :integer          not null, primary key
#  response    :text
#  time_taken  :integer
#  question_id :integer          not null
#  user_id     :integer          not null
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class Answer < ApplicationRecord
  belongs_to :question
  belongs_to :user

  normalize_attributes :response, with: :squish
end
