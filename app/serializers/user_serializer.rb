# == Schema Information
#
# Table name: users
#
#  id                 :integer          not null, primary key
#  first_name         :string
#  last_name          :string
#  email              :string
#  terms_confirmation :boolean
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#

class UserSerializer < ActiveModel::Serializer
  attributes :id
end
