# == Schema Information
#
# Table name: questions
#
#  id          :integer          not null, primary key
#  title       :string
#  description :text
#  time_limit  :integer
#  subject_id  :integer
#  position    :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class QuestionSerializer < ActiveModel::Serializer
  attributes :id
end
