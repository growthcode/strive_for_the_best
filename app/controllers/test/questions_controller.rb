class Test::QuestionsController < ApplicationController
  before_action :set_user
  before_action :set_subject
  before_action :set_question

  # GET /questions/1
  def show
    # binding.pry
    # @answer = @user.answers.new(question_id: @question.id)
  end

  # PATCH/PUT /questions/1
  def update
    binding.pry
  end

  private

  def set_user
    # @user = User.find_by(id: session[:user_id])

    # if @user.blank?
    #   return root_path
    # end
  end

  def set_subject
    @subject = Subject.find(params[:subject_id])
  end

  def set_question
    @question = @subject.questions.find(params[:position])
  end
end
