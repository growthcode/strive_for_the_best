class LandingController < ApplicationController
  before_action :initialize_user, only: :index
  before_action :set_subject_and_question

  def index
  end

  def sign_in
    @user = User.new(user_params)
    if @user.save
      # session[:user_id] = @user.id
      flash[:notice] = "success"
      redirect_to test_subject_ask_question_url(@subject, @question)
    else
      flash[:warning] = "failure"
      render action: :index
    end
  end

  private

  def initialize_user
    @user = User.new
  end

  def set_subject_and_question
    @subject = Subject.first
    @question = @subject.questions.order(:position).first
  end

  def user_params
    params.require(:user).permit(:first_name, :last_name, :email)
  end
end
